﻿namespace Calculator {
    public interface IStackBasedCalculator {
        public short Push(short value);
        public short Pop();
        public short Add();
        public short Sub();
    }
}
