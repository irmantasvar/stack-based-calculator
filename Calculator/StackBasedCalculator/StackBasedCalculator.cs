﻿using System;
using System.Collections.Generic;

namespace Calculator {
    public class StackBasedCalculator : IStackBasedCalculator {
        // This could be calculated from number of bits and could be in the contructor
        private readonly int _maxValue = 1024;
        private readonly int _maxSize;
        private Stack<short> _stack;

        public StackBasedCalculator(int maxSize = 5) {
            if (maxSize < 0) {
                throw new ArgumentException("Max size cannot be negative");
            }

            _maxSize = maxSize;
            _stack = new Stack<short>();
        }

        public short Add() {
            if (_stack.Count < 2) {
                throw new InvalidOperationException("Not enough elements on the stack");
            }

            var value1 = Pop();
            var value2 = Pop();

            return Push((short)(value1 + value2));
        }

        public short Pop() {
            return _stack.Pop();
        }

        public short Push(short value) {
            if (_stack.Count >= _maxSize) {
                throw new InvalidOperationException("Max size reached.");
            }

            var valueToPush = (short)((ushort)value % _maxValue);

            _stack.Push(valueToPush);

            return valueToPush;
        }

        public short Sub() {
            if (_stack.Count < 2) {
                throw new InvalidOperationException("Not enough elements on the stack");
            }

            var value1 = Pop();
            var value2 = Pop();

            return Push((short)(value1 - value2));
        }
    }
}
