﻿using Calculator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Program {
    public class Program {
        private static Dictionary<string, InputType> _operationsDict = new Dictionary<string, InputType> {
            { "push", InputType.Push },
            { "pop", InputType.Pop },
            { "add", InputType.Add },
            { "sub", InputType.Sub },
            { "exit", InputType.Exit },
            { "help", InputType.Help }
        };

        public static void Main(string[] args) {
            var calculator = new StackBasedCalculator();

            PrintHelpMessage();

            while(true) {
                var input = Console.ReadLine();

                var parsedInput = ParseInput(input);

                try {
                    switch (parsedInput.InputType) {
                        case InputType.Push:
                            var pushValue = calculator.Push(parsedInput.InputValue.Value);

                            Console.WriteLine($"Pushed value {pushValue}");
                            break;
                        case InputType.Pop:
                            var popValue = calculator.Pop();

                            Console.WriteLine($"Popped value {popValue}");
                            break;
                        case InputType.Add:
                            var addValue = calculator.Add();

                            Console.WriteLine($"Sum is {addValue}");
                            break;
                        case InputType.Sub:
                            var subValue = calculator.Sub();

                            Console.WriteLine($"Difference is {subValue}");
                            break;
                        case InputType.Exit:
                            return;
                        case InputType.Help:
                            PrintHelpMessage();
                            break;
                        default:
                            Console.WriteLine($"Error occurred: {parsedInput.Error}");
                            break;
                    }
                } catch (Exception ex) {
                    Console.WriteLine($"Error occurred: {ex.Message}");
                }
            }
        }

        private static UserInput ParseInput(string input) {
            var splitInput = input.Split(" ");

            if (splitInput.Length < 1) {
                return new UserInput {
                    Error = "No Input"
                };
            }

            try {
                var inputType = _operationsDict[splitInput.First().ToLower()];

                var userInput = new UserInput {
                    InputType = inputType
                };

                if (inputType == InputType.Push) {
                    if (splitInput.Length < 2) {
                        throw new Exception("Push requires second parameter");
                    }

                    var value = short.Parse(splitInput[1]);
                    userInput.InputValue = value;
                }

                return userInput;
            } catch (KeyNotFoundException _) {
                return new UserInput {
                    Error = $"Unknown operation {splitInput.First()}"
                };
            } catch (FormatException _) {
                return new UserInput {
                    Error = "Invalid format"
                };
            } catch (OverflowException _) {
                return new UserInput {
                    Error = "Value is too big"
                };
            } catch (Exception exception) {
                return new UserInput {
                    Error = exception.Message
                };
            }
        }

        private static void PrintHelpMessage() {
            Console.WriteLine(@"Available operations are:
push <value> - pushes <value> onto stack, <value> must be 10 bit unsigned integer;
pop - pops value from stack;
add - adds two top values from stack;
sub - subtracts second topmost value from topmost value;
exit - exit program;
help - show this message");
        }
    }
}
