﻿namespace Program {
    public class UserInput {
        public InputType? InputType { get; set; }
        public short? InputValue { get; set; }
        public string Error { get; set; }
    }
}
