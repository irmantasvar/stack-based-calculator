﻿namespace Program {
    public enum InputType {
        Push,
        Pop,
        Add,
        Sub,
        Exit,
        Help
    }
}
