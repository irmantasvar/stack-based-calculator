using NUnit.Framework;
using Calculator;
using System;

namespace StackBasedCalculatorTests {
    public class StackBasedCalculatorTests {
        [Test]
        public void Push_PushFiveValues_ReturnsSameValues() {
            var calculator = new StackBasedCalculator();

            for (var i = 0; i < 5; i++) {
                var res = calculator.Push((short)i);

                Assert.AreEqual(i, res);
            }
        }

        [Test]
        public void Push_PushSixValues_ThrowsError() {
            var calculator = new StackBasedCalculator();

            for (var i = 0; i < 5; i++) {
                calculator.Push((short)i);
            }

            Assert.Throws<InvalidOperationException>(() => calculator.Push(0));
        }

        [TestCase(2000, 976)]
        [TestCase(-2, 1022)]
        public void Push_PushInvalidValue_ReturnsModulatedValue(short value, short expectedValue) {
            var calculator = new StackBasedCalculator();

            var result = calculator.Push(value);

            Assert.AreEqual(expectedValue, result);
        }

        [TestCase(10)]
        [TestCase(1023)]
        [TestCase(0)]
        public void Pop_PushValidValue_PopsSameValue(short value) {
            var calculator = new StackBasedCalculator();

            var result = calculator.Push(value);

            Assert.AreEqual(value, result);
        }

        [Test]
        public void Pop_EmptyStack_ThrowsError() {
            var calculator = new StackBasedCalculator();

            Assert.Throws<InvalidOperationException>(() => calculator.Pop());
        }

        [TestCase(2000, 976)]
        [TestCase(-2, 1022)]
        public void Pop_PushInvalidValue_PopsModulatedValue(short value, short expectedValue) {
            var calculator = new StackBasedCalculator();

            calculator.Push(value);

            var result = calculator.Pop();

            Assert.AreEqual(expectedValue, result);
        }

        [TestCase(1, 2, 3)]
        [TestCase(1000, 23, 1023)]
        [TestCase(0, 0, 0)]
        [TestCase(1000, 24, 0)]
        [TestCase(1000, 1000, 976)]
        public void Add_TwoValues_ReturnsSum(short value1, short value2, short expectedValue) {
            var calculator = new StackBasedCalculator();

            calculator.Push(value1);
            calculator.Push(value2);

            var result = calculator.Add();

            Assert.AreEqual(expectedValue, result);
        }

        [TestCase(1, 2, 3)]
        [TestCase(1000, 23, 1023)]
        [TestCase(0, 0, 0)]
        [TestCase(1000, 24, 0)]
        [TestCase(1000, 1000, 976)]
        public void Add_TwoValues_PushesSum(short value1, short value2, short expectedValue) {
            var calculator = new StackBasedCalculator();

            calculator.Push(value1);
            calculator.Push(value2);

            calculator.Add();

            var result = calculator.Pop();
            Assert.AreEqual(expectedValue, result);
        }

        [Test]
        public void Add_NoValuesInStack_ThrowsError() {
            var calculator = new StackBasedCalculator();

            Assert.Throws<InvalidOperationException>(() => calculator.Add());
        }

        [Test]
        public void Add_SingleValueInStack_ThrowsError() {
            var calculator = new StackBasedCalculator();

            calculator.Push(1);

            Assert.Throws<InvalidOperationException>(() => calculator.Add());
        }

        [TestCase(10, 5, 5)]
        [TestCase(1023, 0, 1023)]
        [TestCase(1023, 1023, 0)]
        [TestCase(5, 7, 1022)]
        public void Sub_TwoValues_ReturnsDifference(short value1, short value2, short expectedValue) {
            var calculator = new StackBasedCalculator();

            calculator.Push(value2);
            calculator.Push(value1);

            var result = calculator.Sub();

            Assert.AreEqual(expectedValue, result);
        }

        [TestCase(10, 5, 5)]
        [TestCase(1023, 0, 1023)]
        [TestCase(1023, 1023, 0)]
        [TestCase(5, 7, 1022)]
        public void Sub_TwoValues_PushesDifference(short value1, short value2, short expectedValue) {
            var calculator = new StackBasedCalculator();

            calculator.Push(value2);
            calculator.Push(value1);

            calculator.Sub();

            var result = calculator.Pop();
            Assert.AreEqual(expectedValue, result);
        }

        [Test]
        public void Sub_NoValuesInStack_ThrowsError() {
            var calculator = new StackBasedCalculator();

            Assert.Throws<InvalidOperationException>(() => calculator.Sub());
        }

        [Test]
        public void Sub_SingleValueInStack_ThrowsError() {
            var calculator = new StackBasedCalculator();

            calculator.Push(1);

            Assert.Throws<InvalidOperationException>(() => calculator.Sub());
        }
    }
}